package com.kgc.mgj.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jialin
 * @create 2020-10-01
 */
@Api(tags = "订单接口")
@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @PostMapping(value = "/addOrder")
    public String addOrder(){
        return "add";
    }

}
