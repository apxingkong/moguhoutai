package com.kgc.mgj.dto;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table item_sizeType
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class ItemSizetype {
    /**
     * Database Column Remarks:
     *   主键id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item_sizeType.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   商品尺码类型
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item_sizeType.item_sizeType
     *
     * @mbg.generated
     */
    private Integer itemSizetype;

    /**
     * Database Column Remarks:
     *   商品尺码
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column item_sizeType.item_size
     *
     * @mbg.generated
     */
    private String itemSize;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item_sizeType.id
     *
     * @return the value of item_sizeType.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item_sizeType.id
     *
     * @param id the value for item_sizeType.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item_sizeType.item_sizeType
     *
     * @return the value of item_sizeType.item_sizeType
     *
     * @mbg.generated
     */
    public Integer getItemSizetype() {
        return itemSizetype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item_sizeType.item_sizeType
     *
     * @param itemSizetype the value for item_sizeType.item_sizeType
     *
     * @mbg.generated
     */
    public void setItemSizetype(Integer itemSizetype) {
        this.itemSizetype = itemSizetype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column item_sizeType.item_size
     *
     * @return the value of item_sizeType.item_size
     *
     * @mbg.generated
     */
    public String getItemSize() {
        return itemSize;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column item_sizeType.item_size
     *
     * @param itemSize the value for item_sizeType.item_size
     *
     * @mbg.generated
     */
    public void setItemSize(String itemSize) {
        this.itemSize = itemSize;
    }
}