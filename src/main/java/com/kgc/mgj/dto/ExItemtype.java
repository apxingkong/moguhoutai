package com.kgc.mgj.dto;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table ex_itemType
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class ExItemtype {
    /**
     * Database Column Remarks:
     *   商品类型表主键id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ex_itemType.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   商品类型id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ex_itemType.item_type
     *
     * @mbg.generated
     */
    private Integer itemType;

    /**
     * Database Column Remarks:
     *   商品类型
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ex_itemType.type_name
     *
     * @mbg.generated
     */
    private String typeName;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ex_itemType.id
     *
     * @return the value of ex_itemType.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ex_itemType.id
     *
     * @param id the value for ex_itemType.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ex_itemType.item_type
     *
     * @return the value of ex_itemType.item_type
     *
     * @mbg.generated
     */
    public Integer getItemType() {
        return itemType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ex_itemType.item_type
     *
     * @param itemType the value for ex_itemType.item_type
     *
     * @mbg.generated
     */
    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ex_itemType.type_name
     *
     * @return the value of ex_itemType.type_name
     *
     * @mbg.generated
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ex_itemType.type_name
     *
     * @param typeName the value for ex_itemType.type_name
     *
     * @mbg.generated
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}