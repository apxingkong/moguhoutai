package com.kgc.mgj.mapper;

import com.kgc.mgj.dto.ExItemtype;
import com.kgc.mgj.dto.ExItemtypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExItemtypeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    long countByExample(ExItemtypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int deleteByExample(ExItemtypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int insert(ExItemtype record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int insertSelective(ExItemtype record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    List<ExItemtype> selectByExample(ExItemtypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    ExItemtype selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") ExItemtype record, @Param("example") ExItemtypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") ExItemtype record, @Param("example") ExItemtypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(ExItemtype record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ex_itemType
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(ExItemtype record);
}