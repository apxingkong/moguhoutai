package com.kgc.mgj.utils;

/**
 * @author jialin
 */
public class ReturnResultUtils {
    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMsg("success");
        return returnResult;
    }

    public static <T> ReturnResult returnSuccess(T data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMsg("success");
        returnResult.setData(data);
        return returnResult;
    }

    public static ReturnResult returnFail(int code) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg("fail");
        return returnResult;
    }

    public static ReturnResult returnFailMsg(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;
    }
}
