package com.kgc.mgj.utils;

import java.util.UUID;

/**
 * @author jialin
 * @createTime 2020-09-03
 */
public class UuidUtils {
    //sessionId
    /*String sessionId = session.getId();
    Long timestamp = System.currentTimeMillis();
    sysUserUuid.setId(timestamp + sessionId.substring(0,12));*/

    //生成id，时间戳+12位UUID
    //排序：order by mid(id , 1 , 13)  -> 取出前13个字符，根据时间排序
    //Long timestamp = System.currentTimeMillis();
    //String uuid = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 12);

    //获取16位UUID
    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    //获取13位时间戳+12位UUID
    public static String getTimeUUID(){
        return System.currentTimeMillis() + UUID.randomUUID().toString().replaceAll("-", "").substring(0,12);
    }
}
