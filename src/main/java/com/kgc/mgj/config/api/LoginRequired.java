package com.kgc.mgj.config.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 9:21
 * 添加该注解的类中所有接口必须登录后才能访问
 * 该注解也可以单独添加到方法上
 */
@Target({ElementType.METHOD,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginRequired {
}
