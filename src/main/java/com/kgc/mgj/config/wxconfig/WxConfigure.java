package com.kgc.mgj.config.wxconfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("wx.login")
public class WxConfigure {
    private String appID;
    private String redirectUri;
    private String codeUri;

    private String accessTokenUrl;
    private String secret;

    private String userInfoUrl;
    private String lang;

    private String openIdUrl;


    public String getCode() {
        StringBuffer stringBuffer = new StringBuffer(getCodeUri());
        stringBuffer.append("appid=").append(appID);
        stringBuffer.append("&redirect_uri=").append(redirectUri);
        stringBuffer.append("&response_type=code");
        stringBuffer.append("&scope=snsapi_userinfo");
        stringBuffer.append("#wechat_redirect");
        return stringBuffer.toString();
    }

    public String getAccessToken(String code) {
        StringBuffer stringBuffer = new StringBuffer(getAccessTokenUrl());
        stringBuffer.append("appid=").append(appID);
        stringBuffer.append("&secret=").append(secret);
        stringBuffer.append("&code=").append(code);
        stringBuffer.append("&grant_type=authorization_code");
        return stringBuffer.toString();
    }

    public String getUserInfo(String access_token,String openid) {
        StringBuffer stringBuffer = new StringBuffer(getUserInfoUrl());
        stringBuffer.append("access_token=" + access_token);
        stringBuffer.append("&openid=" + openid);
        stringBuffer.append("&lang="+lang);
        return stringBuffer.toString();
    }

    public String getOpenId(String jscode){
        StringBuilder stringBuilder = new StringBuilder(getOpenIdUrl());
        stringBuilder.append("appid=" + appID);
        stringBuilder.append("&secret=" + secret);
        stringBuilder.append("&js_code=" + jscode);
        stringBuilder.append("&grant_type=authorization_code");
        return stringBuilder.toString();
    }

}
