package com.kgc.mgj.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author jialin
 */
@Configuration
@MapperScan(basePackages = "com.kgc.mgj.mapper")
public class MybatisConfig {
}
